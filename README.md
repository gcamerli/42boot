## **42**

_Hitchhiker's guide to 42 Piscine_ [ **[42boot.gcamer.li](https://42boot.gcamer.li)** ]

[![pipeline status](https://gitlab.com/gcamerli/42boot/badges/master/pipeline.svg)](https://gitlab.com/gcamerli/42boot/commits/master)


![Don't Panic](themes/hugo-material-docs/static/images/dont_panic.jpg)

### **Description**

This Hitchhiker's guide to 42 Piscine is divided into 5 small sections:

+ **Intro**

+ **Getting started**

+ **Bootcamp**

+ **Examshell**

+ **Ending**

Happy Hacking!

### **Credits**

This little guide is built with [Hugo](https://gohugo.io) and hosted by [Gitlab](https://gitlab.com) with love.

**Theme**: [Material Docs](https://themes.gohugo.io/theme/material-docs/)  

### **CC License**

This work is provided under the terms of this license: [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)

### **Donations**

Donations are accepted at:

+ **BTC**: 152pyNwPRr22XWYdTuyHormznzu4PvPrWg
