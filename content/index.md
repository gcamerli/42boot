---
date: 2017-05-08T18:42:00+01:00
title: Intro
type: index
weight: 10
tags:
- 42
---

In **2013** a group of 4 visionary friends set up a unique **educational** reality based in **Paris**.

**Code name**: **[42](http://42.fr)**

If you have ever heard about the great story **[Hitchhiker's guide to the galaxy](https://en.wikipedia.org/wiki/The_Hitchhiker%27s_Guide_to_the_Galaxy)**,
you're in the right place. And for sure you shold read it ;)

![Hitchhikers](images/hitchhikers.jpg)

Sometimes fictions can enrich reality and bring forth **extraordinary** stories.

{{< note title="Hint" >}}
If you get lost during the **hard time** that expects to you at **42**, please consider
this my **point of view** as a personal **gift** to you.
{{< /note >}}

Good luck and **enjoy** the time!

## **Pedagogy**

> **42**'s directors have proven that a **rigorous**, **open curriculum**, one that actively involves students in passionate and **collaborative** projects, is the type of training method that forms the most inspired **developers** and **computer scientists**.

At the beginning you could be confused or skeptical, but when you touch the **real innovation**
of this kind of pedagogy you will see the **difference** with other **self learning** paths. Besides it's **completely free**!

If you want to read more: **[42 Pedagogy](https://www.42.us.org/our-philosophy/pedagogical-innovation/)**

## **p2p**

> **No classes, no professors**: at **42**, the students are the ones in charge of their success and that of their classmates, and is centered around a curriculum which is **100% practical** and **project based**.

As in all **distributed systems**, such like **BitTorrent** or **Bitcoin**, also in **42**
is spreaded a **collaborative** process through each **person** at each level. You will learn
to **code** but firstly you will learn how to **interact** with other **people** to reach this.

If you want to read more: **[42 Peer to peer](https://www.42.us.org/our-philosophy/pedagogical-innovation/peer-to-peer-learning/)**

## **Piscine**

> We call it the “**piscine**” (French for “**swimming pool**”) because you will be thrown into a totally new and **immersive** experience, in which only **motivation**, **willpower** and **hard work** will keep you afloat. This **4-week intensive** basic training is an incredibly rewarding, **once in-a-lifetime experience**. The “**piscine**” is the **last step** of the selection process.

You've been selected after you succeeded **online tests**, already **checked-in** and you're deciding
when it will get started your **Piscine**. Trust me, you don't know what's waiting for you.
Get prepared and remember: the answer is **42**.

If you want to read more: **[42 Piscine](https://www.42.us.org/program/piscine/)**
