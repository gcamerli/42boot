---
date: 2017-05-08T19:42:00+01:00
title: Getting started
weight: 20
---

> _Who exalts himself in good times, will waver in adversity._
>
> (Hagakure)

## **Learning**

To getting started you have to **put your hands** on:

+ **Command line**
+ **Text editor**
+ **Version control**

**Don't touch the unicorns please!**

![Unicorn](../images/deadpool.jpg)

During this **learning process** you will recognize the level of your **technical sophistication**.

To **go deeper** it might be very useful to know some **key concepts** about:

+ **UNIX**
+ **Programming principles**

But let's proceed with **order**.

## **Resources**

**[Learn Enough to Be Dangerous](https://www.learnenough.com/)**

Here you can feel (**not unicorns!**) your first shiver or not, it **depends on you**.
But surely you can learn enough on:

+ [Command line](https://www.learnenough.com/command-line-tutorial) (**bash**)
+ [Text editor](https://www.learnenough.com/text-editor-tutorial) (**vim**)
+ [Version control](https://www.learnenough.com/git-tutorial) (**git**)

**[Cloud 9](https://c9.io/)**

To set up your **development environment (IDE)** without getting crazy you can opt for a simple
**cloud** solution. Here you can quickly build a **Linux environment** for free, full of what you need to
getting started.

**[Virtual Box](https://www.virtualbox.org/)**

If you want a **local environment** you can set up a full **virtual machine** without partitioning
your hard drive.

{{< warning title="Only for unicorns lovers" >}}
**Now you can touch them**.  ^____^
{{< /warning >}}

To go deeper with **git**:

+ **[Getting Git Right](https://www.atlassian.com/git)**

+ **[Pro Git](https://git-scm.com/book/en/v2)**

+ **[Learn Git Branching](https://learngitbranching.js.org/)**

+ **[Git? Tig!](https://www.atlassian.com/blog/git/git-tig)**

To discover how deep the **rabbit hole** goes:

+ **[Manjaro Linux](https://manjaro.org/)** (rolling release based on **Arch Linux**)

+ **[Linux Academy](https://www.youtube.com/playlist?list=PLv2a_5pNAko0qXMS465ynosFwEx-ZMclw)** (video resources to get **more confidence** after the first steps)

If you get in troubles you have to fix on your own the issues. **Unicorns are not for everyone**. ^____^
