---
date: 2017-05-08T19:42:00+01:00
title: Bootcamp
weight: 30
---

> _Put your foot in foul and fall seven times, eight raised and rise._
>
> (Hagakure)

**Piscine** is a **dev bootcamp**, an immersive programming experience of **4 weeks** long from monday to sunday **no stop**. The main advice you have to remember is simple: **sleep well, eat healty, work hard**.

![Sink or swim](../images/sink_or_swim.jpg)

**Understand** the problem, search for a solution, ask to your mates, cooperate and mostly **TEST**, **TEST**, **TEST** your code.

{{< note title="Hint" >}}
**42 staff** doesn't require any kind of **knowledge** to deal with this **bootcamp**. All this stuff is an **option** you can consider at **your own risk**. It's just based on my **personal experience**, so it doesn't replace the **commitment** that will be required during your **Piscine**.
{{< /note >}}

## **UNIX**

_With great power there must also come great responsibility._

![Unix](../images/unix.jpg)

**UNIX** is a huge family of **operating systems** from the 70s. **UNIX-like** variants are:

+ **[Linux](https://en.wikipedia.org/wiki/Linux)**
+ **[MacOS](https://en.wikipedia.org/wiki/MacOS)**

During the whole bootcamp you will use a lot all this stuff.

## **Shell**

_Where there's a shell, there's a way._

Did you like **[Learn enough to be dangerous](https://www.learnenough.com/courses)** fundamentals from **Michael Hartl**? Haven't you done that yet? Go back and **take a look**, they could give you a **good start**. This will be your **daily bread**:

+ **[vim](http://vimsheet.com/)**

+ **[git](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf)**

+ **[bash](http://ryanstutorials.net/linuxtutorial/cheatsheet.php)**

These **cheat-sheets** can be useful. **DON'T PANIC** you don't have to learn all the commands at once but **understand** them well **step by step**. Try to get used to it as much as possible by doing all the tests. **Practice makes perfect**.

```bash
$ echo "May the unicorns be with you."
```

## **C**

After the first 2 days you will deal with **C** programming. Are you stuck? Don't give up at once, **C** is one of the most robust **compiled language**. Take your time.

Each **UNIX** based operating system is already equipped with several tools.

> The system's basic components include the **GNU Compiler Collection (GCC)**, the **GNU C library (glibc)**, and **GNU Core Utilities (coreutils)**, but also the **GNU Debugger (GDB)**, **GNU Binary Utilities (binutils)**, the **GNU Bash shell** and the GNOME desktop environment. GNU developers have contributed to **Linux** ports of GNU applications and utilities, which are now also widely used on other operating systems such as **BSD** variants, **Solaris** and **macOS**.

> (Source: [Wikipedia](https://en.wikipedia.org/wiki/GNU))

**Resources**

If you already know some **programming languages** it could help, but you have to understand that this **bootcamp** has been designed to allow anyone, also with **zero knowledge**, to deal with this **hard way**. **42** has its own built-in method, follow it **thoroughly**.

Anyway this beginner's guide from **Raspberry Pi Foundation** could help:

+ **[Learn to code with C](https://www.raspberrypi.org/magpi/learn-code-c/)**

As the author wrote about **C**:

> It can give you **control over the smallest details** of
how a processor operates, but is still **simple to
learn and read**.

> (Simon Long)

For sure you can use the **dev environment** discussed in the previous section. So don't hesitate and **put your hands on** it.

{{< warning title="Remember" >}}
**Copypasta** is made to ruin every last bit of originality. So **don't cheat** if you don't want to get a penalty. **You've been warned**.
{{< /warning >}}
