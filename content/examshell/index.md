---
date: 2017-05-08T19:42:00+01:00
title: Examshell
weight: 40
---

> _Now is the present moment and the present moment is now._
>
> (Hagakure)

Throughout the bootcamp you will always have access to the **Internet**.
You can search and compare all the **informations** you want also with the other mates. No **restrictions**.

But **once a week** you will have to prove that you are able to **program** on your own.
So **train** well before because during the exams you will be **offline**. Don't be stupid **;)**

Don't you remember something? **[RTFM](https://en.wikipedia.org/wiki/RTFM)** will be your mantra.

## **Timing**

![Hurry up](../images/white_rabbit.jpg)

**Explore everything**

Think like an **hacker** and after your login navigate all the **filesystem**, you will have less than
**10 minutes** to understand what to do before the session expires. If you fail, your exam will end even before you start. So **hurry up** and look at the **time**.

**Kerberos**

Do you remember **[Kerberos](https://en.wikipedia.org/wiki/Kerberos_%28protocol%29)**? It will be your best friend during the **exams**. So
understand well the **basics**.

```bash
$ kinit username #To create a new ticket using your username
```

**Resources**

+ **[UNIX Kerberos Commands](https://uit.stanford.edu/service/kerberos/user_guide/commands)**

You will love it. ^____^

## **C**

If you're good enough you can finally start the **examshell**. You will deal with:

+ **Shell Scripts**
+ **C Functions**
+ **C Programs**

Difficulty level is **increasing**. You can move to the next level just succeeding
the previous. When you fail you can try again by solving **another problem** with the same
difficulty level but with a lower score. When you finish the **number of attempts**
your exam will end automatically, generating your **final score**.

**AI** at **42** is strict and clever, so do the right thing and **remember**:

![Don't Panic](../images/marvin.jpg)
