---
date: 2017-05-08T19:42:00+01:00
title: Ending
weight: 50
---

> _The hours of deep sleep and awakening are very important._
>
> (Hagakure)

## **Rigor**

At a first glance it will seem **overwhelming** but you will soon recognize the importance
of:

+ Observe the **standard**
+ Keep the **code clean**

And you will **respect them** if you don't want to sink. ;)

**Norminette**

To check if everything is fine before a **push**, always remember:

```bash
$ norminette -R CheckForbiddenSourceHeader #norminette flag
```

**Moulinette**

To compile properly each **C** program:

```bash
$ gcc -Wall -Wextra -Werror [program.c] #gcc compilation flags - [program.c] is generic ;)
```

If you see a **charming cat** inside the **42** building, that's **[Norminet](https://twitter.com/42norminet)**!

## **AI and Algorithms**

![42](../images/42.jpg)

Ever since you go inside, from the first to the last day, you will breathe a **distributed intelligence** that will tell you how much the relationship between **man and machine** can create a better environment where you can **learn**, **discover**, and **spread creativity** in a **unique** way. Having also a lot of **fun**. :D

**42** takes care a lot of **Artificial Intelligence** and **Algorithms** and you'll be able to taste both during the entire **bootcamp**.

So have a **good journey**.

## **Happy Hacking**

![Hack the planet](../images/hack_planet.jpg)

Now put on your sunglasses, take your keyboard and **HACK the planet**!
